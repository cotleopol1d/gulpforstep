import fileInclude from "gulp-file-include";
import webpHtmlNoSvg from "gulp-webp-html-nosvg";
export const html = (done) => {
    app.gulp.src(app.path.src.html)
       .pipe(app.plugins.plumber(app.plugins.notify.onError({
           title: "HTML",
           massage: "Error: <%= error.message %>"
       })))
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(app.plugins.replace(/..\/img/g, './dist/img'))
        .pipe(webpHtmlNoSvg())
        .pipe(app.gulp.dest(app.path.build.html))
       .pipe(app.plugins.browserSync.stream());
    done();
}