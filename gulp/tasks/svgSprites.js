import svgSprive from "gulp-svg-sprite";
export const svgSprites = (done)=> {
    app.gulp.src(app.path.src.svgIcons, {sourcemaps: true})
        .pipe(app.plugins.plumber(app.plugins.notify.onError({
            title: "SVG",
            massage: "Error: <%= error.message %>"
        })))
        .pipe(svgSprive({
            mode: {
                stack: {
                    sprite: `../icons/icons.svg`,
                    example:false,
                }
            }
            })
        )
        .pipe(app.gulp.dest(app.path.build.images));
    done();
}
